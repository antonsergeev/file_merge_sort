#pragma once

#include <ostream>
#include <string>

#include <boost/optional.hpp>
#include <boost/program_options.hpp>

class SorterOptions
{
public:
    SorterOptions(int argc, const char* argv[])
    {
        namespace po = boost::program_options;
        description_.add_options()
            ("help,h", po::value<std::string>(), "Help message")
            ("input,i", po::value<std::string>(), "Input file, stdin by default")
            ("output,o", po::value<std::string>(), "Output file, stdout by default")
            ("buffer", po::value<size_t>(), "Buffer size, MB");

        po::store(po::parse_command_line(argc, argv, description_), variables_map_);
        po::notify(variables_map_);
    }
    bool help_requested() const
    {
        return variables_map_.count(help_option_);
    }
    void print_description(std::ostream& out) const
    {
        out << description_ << std::endl;
    }
    size_t get_buffer_size() const
    {
        return variables_map_.count(buffer_option_) ? variables_map_.at(buffer_option_).as<size_t>() : default_buffer_size_;
    }
    boost::optional<std::string> get_input_file_name() const
    {
        if(variables_map_.count(input_option_))
            return variables_map_.at(input_option_).as<std::string>();
        return boost::none;
    }
    boost::optional<std::string> get_output_file_name() const
    {
        if(variables_map_.count(output_option_))
            return variables_map_.at(output_option_).as<std::string>();
        return boost::none;
    }
private:
    boost::program_options::options_description description_ = { "Pairs sorter" };
    boost::program_options::variables_map variables_map_;

    static constexpr size_t default_buffer_size_ = 200'000'000;

    static constexpr const char help_option_[] = "help";
    static constexpr const char buffer_option_[] = "buffer";
    static constexpr const char input_option_[] = "input";
    static constexpr const char output_option_[] = "output";
};
