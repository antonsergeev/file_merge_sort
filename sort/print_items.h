#pragma once

#include <cstdio>
#include <cstring>

#include "item.h"

namespace sort {

inline void print_entry(std::FILE* storage_file, Item const& item, const bool new_line = false)
{
    std::fwrite(item.entry, sizeof(char), std::strlen(item.entry) + (new_line ? 0 : 1), storage_file);
    if (new_line)
        std::fwrite("\r\n", 1, 2, storage_file);
}

}
