#pragma once

#include <cassert>
#include <cstdio>
#include <memory>
#include <queue>

#include "common/file_holder.h"

#include "sorted_block.h"

namespace sort {
class KWayFileMerge
{
public:
    explicit KWayFileMerge(const size_t item_buffer_size)
        : item_buffer_size_(item_buffer_size)
    {}

    void load(std::FILE* input_file)
    {
        size_t cache_size = 500'000;
        if (_fseeki64(input_file, 0, SEEK_END) == 0)
        {
            size_t const file_size = _ftelli64(input_file);
            rewind(input_file);
            if (file_size > item_buffer_size_)
                cache_size = item_buffer_size_ * item_buffer_size_ / file_size;
        }

        auto block_buffer = std::make_unique< ItemBuffer >(item_buffer_size_);

        const auto tmp_file = tmp_file_.get();
        for (;;)
        {
            if (!block_buffer->load(input_file))
                break;

            if (!block_buffer->get_items().empty())
            {
                block_buffer->sort();
                auto new_node = std::make_shared<SortedBlock>(block_buffer->get_items(), tmp_file, cache_size);
                k_way_merge_queue_.push(std::move(new_node));
            }
        }
    }

    void extract(std::FILE* output)
    {
        while (!k_way_merge_queue_.empty())
        {
            SortedBlockPtr top_node = k_way_merge_queue_.top();
            assert(!top_node->empty());
            Item top_item = top_node->min();
            print_entry(output, top_item, true);
            k_way_merge_queue_.pop();
            top_node->pop();
            if (!top_node->empty())
            {
                k_way_merge_queue_.push(std::move(top_node));
            }
        }
    }
private:
    class SortedBlockPtrComparator
    {
    public:
        bool operator() (SortedBlockPtr const& lhs, SortedBlockPtr const& rhs)
        {
            return rhs->min() < lhs->min();
        }
    };

    const size_t item_buffer_size_;
    const FileHolder tmp_file_ = create_file_holder(std::tmpfile());
    using QueueType = std::priority_queue< SortedBlockPtr, std::vector<SortedBlockPtr>, SortedBlockPtrComparator>;
    QueueType k_way_merge_queue_;
};
}
