#pragma once

#include <cstdio>
#include <cstdlib>

#include "item.h"

namespace sort {

inline bool is_delimiter(const char delimiter) // faster than boost::algorithm::any_of_equal
{
    return delimiter == '\n' || delimiter == '\r' || delimiter == '\0';
}

inline size_t fill(std::FILE* input_file, size_t max_read, char* buffer, size_t buffer_size) // to read_blob/block
{
    size_t size = std::fread(buffer, sizeof(char), std::min(max_read, buffer_size - 1), input_file);
    if (!size)
        return 0;
    if (!std::feof(input_file))
    {
        size_t last = size - 1;
        for (; last != 0 && !is_delimiter(buffer[last]); --last);
        if (last != size - 1)
        {
            _fseeki64(input_file, -static_cast<long long>(size - 1 - last), SEEK_CUR);
            size = last + 1;
        }
    }
    buffer[size++] = '\0';
    return size;
}

template<typename InputIterator, typename OutputIterator>
void populate(InputIterator input_begin, InputIterator input_end, OutputIterator output)
{
    InputIterator cursor = input_begin;
    for (;;)
    {
        assert(cursor <= input_end);
        for (; cursor != input_end && is_delimiter(*cursor); *cursor++ = '\0');
        if (cursor == input_end)
            break;
        assert(cursor < input_end);
        Item item;
        item.entry = cursor;
        item.id = std::strtoul(cursor, &cursor, 10);
        item.sentence = ++cursor;
        assert(cursor < input_end);
        for (; cursor != input_end && !is_delimiter(*cursor); ++cursor);
        assert(cursor <= input_end);
        *output++ = item;
    }
}

}
