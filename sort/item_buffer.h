#pragma once

#include <algorithm>
#include <cstdio>
#include <iterator>

#include "item.h"
#include "parse_items.h"

namespace sort {

class ItemBuffer
{
public:
    explicit ItemBuffer(const size_t size_bytes)
        : size_bytes_(size_bytes)
        , buffer_{ new char[size_bytes] }
    {
        const size_t expected_items_count = size_bytes_ / 600; // TODO: avoid magic number
        items_.reserve(expected_items_count);
    }
    bool load(FILE * input_file)
    {
        return load(input_file, size_bytes_);
    }
    bool load(FILE * input_file, size_t max_read)
    {
        const size_t size = fill(input_file, max_read, buffer_, size_bytes_);
        if (!size)
            return false;
        items_.clear();
        populate(buffer_, buffer_ + size, std::back_inserter(items_));
        return true;
    }
    void sort()
    {
        std::sort(items_.begin(), items_.end());
    }
    ItemVector const& get_items()
    {
        return items_;
    }
    ~ItemBuffer()
    {
        delete[] buffer_;
    }
private:
    const size_t size_bytes_;
    char * const buffer_;
    ItemVector items_;
};

}
