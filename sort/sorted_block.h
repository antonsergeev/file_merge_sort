#pragma once

#include <algorithm>
#include <cstdio>
#include <memory>
#include <vector>

#include "item_buffer.h"
#include "print_items.h"

namespace sort {

class SortedBlock
{
public:
    SortedBlock(ItemVector const& sorted_items, std::FILE* storage_file, const size_t cache_size)
        : max_cache_blob_size_(cache_size)
        , cache_blob_{ new char[max_cache_blob_size_] }
    {
        constexpr size_t expected_items_count = 200;
        cached_items_.reserve(expected_items_count);
        
        file_segment_.file = storage_file;

        auto item_it = sorted_items.begin();
        for (; item_it != sorted_items.end() && cache_item(*item_it); ++item_it);
        current_cached_item_it_ = cached_items_.begin();

        file_segment_.begin = _ftelli64(storage_file);
        for (; item_it != sorted_items.end(); ++item_it)
        {
            print_entry(storage_file, *item_it);
        }

        file_segment_.end = _ftelli64(storage_file);
        assert(!empty());
    }

    Item min() const
    {
        return *current_cached_item_it_;
    }

    void pop()
    {
        assert(!cached_items_.empty());
        if (++current_cached_item_it_ == cached_items_.end())
        {
            fill_cache();
        }
    }

    bool empty() const
    {
        assert ((current_cached_item_it_ == cached_items_.end()) == cached_items_.empty());
        return cached_items_.empty();
    }
    ~SortedBlock()
    {
        delete[] cache_blob_;
    }
private:
    bool cache_item(Item const& item)
    {
        const size_t entry_length = std::strlen(item.entry);
        if (entry_length + 1 + cache_blob_size_ > max_cache_blob_size_)
            return false;
        char* const destination = cache_blob_ + cache_blob_size_;
        std::copy(item.entry, item.entry + entry_length + 1, destination);
        cached_items_.push_back(Item{ destination, destination + (item.sentence - item.entry), item.id });
        cache_blob_size_ += entry_length + 1;

        return true;
    }

    void fill_cache()
    {
        _fseeki64(file_segment_.file, file_segment_.begin, SEEK_SET);
        cache_blob_size_ = fill(file_segment_.file, static_cast<size_t>(file_segment_.end - file_segment_.begin), cache_blob_, max_cache_blob_size_);

        cached_items_.clear();
        populate(cache_blob_, cache_blob_ + cache_blob_size_, std::back_inserter(cached_items_));
        assert(std::is_sorted(cached_items_.begin(), cached_items_.end()));
        current_cached_item_it_ = cached_items_.begin();
        file_segment_.begin = _ftelli64(file_segment_.file);
    }

    struct FileSegment
    {
        std::FILE * file;
        using OffsetType = long long;
        OffsetType begin;
        OffsetType end;
    } file_segment_;

    const size_t max_cache_blob_size_;
    char* cache_blob_;
    size_t cache_blob_size_ = 0;
    ItemVector cached_items_;
    ItemVector::const_iterator current_cached_item_it_;
};

using SortedBlockPtr = std::shared_ptr< SortedBlock >;

}
