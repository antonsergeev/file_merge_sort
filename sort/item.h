#pragma once

#include <cstring>
#include <vector>

namespace sort {

struct Item
{
    const char* entry;
    const char* sentence;
    unsigned id;
};

using ItemVector = std::vector<Item>;

inline bool operator < (Item const& lhs, Item const& rhs)
{
    const int compare = std::strcmp(lhs.sentence, rhs.sentence);
    return compare < 0 || (compare == 0 && lhs.id < rhs.id); // leaving brackets for readability
}

}