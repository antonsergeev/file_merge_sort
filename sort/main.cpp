#include <cassert>
#include <iostream>
#include <string>

#include "common/elapsed_time.h"

#include "k_way_file_merge.h"
#include "sorter_files_holder.h"
#include "sorter_options.h"

using namespace sort;

int main(int argc, const char* argv[])
{
    ElapsedTime elapsed_time;
    const SorterOptions sorter_options(argc, argv);

    if (sorter_options.help_requested())
    {
        sorter_options.print_description(std::cout);
        return 0;
    }

    const SorterFilesHolder files_holder{ sorter_options.get_input_file_name(), sorter_options.get_output_file_name() };

    KWayFileMerge entry_merge(sorter_options.get_buffer_size());
    entry_merge.load(files_holder.get_input_file());
    const auto output_file = files_holder.get_output_file();
    std::rewind(output_file);
    entry_merge.extract(output_file);

    if(output_file != stdout)
        elapsed_time.print(std::cout);

    return 0;
}