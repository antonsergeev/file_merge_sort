#pragma once

#include <cstdio>
#include <string>
#include <boost/optional.hpp>

#include "common/file_holder.h"

class SorterFilesHolder
{
public:
    using MaybeString = boost::optional<std::string>;
    SorterFilesHolder(MaybeString const& input_file_name, MaybeString const& output_file_name)
    {
        if (input_file_name == output_file_name && input_file_name)
        {
            input_file = output_file = create_file_holder( std::fopen(input_file_name->c_str(), "rb+") );
        }
        else
        {
            input_file = create_file_holder(input_file_name ? std::fopen(input_file_name->c_str(), "rb") 
                                                            : std::freopen(nullptr, "rb", stdin));
            output_file = create_output_holder(output_file_name);
        }
        assert(input_file);
        assert(output_file);
    }
    std::FILE* get_input_file() const
    {
        return input_file.get();
    }
    std::FILE* get_output_file() const
    {
        return output_file.get();
    }
private:
    FileHolder input_file;
    FileHolder output_file;
};