#pragma once

#include <string>

#include <boost/optional.hpp>
#include <boost/program_options.hpp>

class GeneratorOptions
{
public:
    GeneratorOptions(int argc, const char* argv[])
    {
        namespace po = boost::program_options;

        description_.add_options()
            ("help,h", po::value<std::string>(), "Help message")
            ("output,o", po::value<std::string>(), "Output file, stdout by default")
            ("size", po::value<size_t>(), "Approximate file size");

        po::store(po::parse_command_line(argc, argv, description_), variables_map_);
        po::notify(variables_map_);
    }
    bool help_requested() const
    {
        return variables_map_.count(help_option_);
    }
    void print_description(std::ostream& out) const
    {
        out << description_ << std::endl;
    }
    size_t get_output_size() const
    {
        return variables_map_.count(size_option_) ? variables_map_.at(size_option_).as<size_t>() : default_output_size_;
    }
    boost::optional<std::string> get_output_file_name() const
    {
        if (variables_map_.count(output_option_))
            return variables_map_.at(output_option_).as<std::string>();
        return boost::none;
    }
private:
    boost::program_options::options_description description_ = { "Pairs generator" };
    boost::program_options::variables_map variables_map_;

    static constexpr size_t default_output_size_ = 1'000'000;

    static constexpr const char help_option_[] = "help";
    static constexpr const char size_option_[] = "size";
    static constexpr const char output_option_[] = "output";
};
