#include <iostream>
#include <memory>

#include "common/elapsed_time.h"
#include "common/file_holder.h"

#include "generator_options.h"
#include "sample_generator.h"

int main(int argc, const char* argv[])
{
    ElapsedTime elapsed_time;

    const GeneratorOptions generator_options(argc, argv);
    if (generator_options.help_requested())
    {
        generator_options.print_description(std::cout);
        return 0;
    }

    const auto output_file_holder = create_output_holder(generator_options.get_output_file_name());

    auto sample_generator = std::make_unique< SampleGenerator >(generator_options.get_output_size());

    (*sample_generator)(output_file_holder.get());

    elapsed_time.print(std::cout);

    return 0;
}