#pragma once

class UnsignedGenerator
{
public:
    explicit UnsignedGenerator(const unsigned max = std::numeric_limits<unsigned>::max())
        : index_generator_{ 0, max }
    {}
    unsigned operator()()
    {
        return index_generator_(mt_);
    }
private:
    std::mt19937 mt_{ std::random_device{}() };
    std::uniform_int_distribution<unsigned> index_generator_;
};