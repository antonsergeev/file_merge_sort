#pragma once

#include <cstdio>

#include "entry_generator.h"

class SampleGenerator
{
public:
    using FileSizeBytes = unsigned long long;

    SampleGenerator(const FileSizeBytes target_file_size_bytes)
        : target_file_size_bytes_(target_file_size_bytes){}

    void operator()(FILE* output_file)
    {
        const char* index_format = "%u.";
        constexpr size_t entry_length_upper_bound = EntryGenerator::max_sentence_length + 30;
        size_t bytes_written = 0;
        char* cursor = buffer;
        for (; bytes_written + cursor - buffer < target_file_size_bytes_;)
        {
            if (std::end(buffer) - cursor < entry_length_upper_bound)
            {
                bytes_written += std::fwrite(buffer, sizeof(char), cursor - buffer, output_file);
                cursor = buffer;
            }
            cursor = entry_generator_.print(cursor, index_format);
            index_format = "\r\n%u.";
        }
        std::fwrite(buffer, sizeof(char), cursor - buffer, output_file);
    }
private:
    
    static constexpr size_t buffer_size = 10'000'000;
    char buffer[buffer_size];
    EntryGenerator entry_generator_;
    const FileSizeBytes target_file_size_bytes_;
};