#pragma once

#include <string>

#include <boost/algorithm/cxx11/iota.hpp>

namespace detail {

static inline std::string symbol_range(const char first, const char last) // closed range
{
    assert(first <= last);
    std::string sequence(last - first + 1, '\0');
    boost::algorithm::iota(sequence, first);
    return sequence;
}

static inline std::string symbols_string()
{
    return " !\"'(),-./:;?[]{}" + symbol_range('0', '9') + symbol_range('A', 'Z') + symbol_range('a', 'z');
};

}
