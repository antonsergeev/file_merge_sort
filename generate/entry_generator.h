#pragma once

#include <algorithm>

#include "symbol_generator.h"
#include "unsigned_generator.h"

#include "detail/symbols.h"

class EntryGenerator
{
public:
    static constexpr unsigned max_sentence_length = 1024;

    char * print(char* cursor, const char* index_format)
    {
        cursor += std::sprintf(cursor, index_format, index_generator());
        return std::generate_n(cursor, length_generator(), [&symbol_generator = symbol_generator_] () { return symbol_generator(); });
    }
private:
    SymbolGenerator symbol_generator_{ detail::symbols_string() };
    UnsignedGenerator index_generator{};
    UnsignedGenerator length_generator{ max_sentence_length };
};
