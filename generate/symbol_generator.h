#pragma once

#include <string>
#include <random>

class SymbolGenerator
{
public:
    explicit SymbolGenerator(std::string const& symbols_string)
        : symbol_count_{ symbols_string.length() }
        , pack_parameters_{ get_pack_parameters(symbol_count_) }
        , symbol_generator_{ 0, pack_parameters_.max_pack_value }
    {
        assert(symbol_count_);
        assert(symbol_count_ <= max_symbols);
        symbols_string.copy(sentence_symbols_.data(), std::size(sentence_symbols_));
    }
    char operator()()
    {
        if (!left_in_pack_--)
        {
            pack_ = symbol_generator_(mt64_);
            left_in_pack_ = pack_parameters_.max_packed_symbols - 1;
        }
        const size_t generated_index = pack_ % symbol_count_;
        pack_ /= symbol_count_;
        return sentence_symbols_[generated_index];
    }
private:
    struct PackParameters
    {
        using PackValue = unsigned long long;
        int max_packed_symbols;
        PackValue max_pack_value;
    };

    static PackParameters get_pack_parameters(const size_t symbol_count)
    {
        PackParameters::PackValue max_pack_value = symbol_count - 1;
        int max_packed_symbols = 0;
        auto const next = [symbol_count] (auto value) { return (value + 1) * symbol_count - 1; };
        for (; next(max_pack_value) > max_pack_value /*no overflow*/; max_pack_value = next(max_pack_value), ++max_packed_symbols);

        return{ max_packed_symbols, max_pack_value };
    }

    static constexpr size_t max_symbols = 256;
    const size_t symbol_count_;
    PackParameters pack_parameters_;
    std::mt19937_64 mt64_{ std::random_device{}() };
    std::uniform_int_distribution<PackParameters::PackValue> symbol_generator_;
    std::array< char, max_symbols > sentence_symbols_;
    int left_in_pack_ = 0;
    PackParameters::PackValue pack_;
};