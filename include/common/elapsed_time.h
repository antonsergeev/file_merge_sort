#pragma once

#include <chrono>
#include <iomanip>

class ElapsedTime
{
public:
    long long get_milliseconds() const
    {
        const auto current = std::chrono::steady_clock::now();
        return std::chrono::duration_cast< std::chrono::duration< long long, std::milli > >(current - start_).count();
    }
    void print(std::ostream& out) const
    {
        const auto milliseconds = get_milliseconds();
        constexpr int ms_in_min = 1000 * 60;

        out << milliseconds / ms_in_min << " min " << std::setprecision(3)
            << milliseconds % ms_in_min / 1000. << " sec";
    }
private:
    std::chrono::steady_clock::time_point start_ = std::chrono::steady_clock::now();
};