#pragma once

#include <cstdio>
#include <memory>
#include <string>

#include <boost/optional.hpp>

using MaybeFileName = boost::optional<std::string>;

using FileHolder = std::shared_ptr< std::FILE >;
FileHolder create_file_holder(FILE* file)
{
    return FileHolder(file, &std::fclose);
}

FileHolder create_output_holder(MaybeFileName const& output_file_name) {
    return FileHolder(
        output_file_name ? std::fopen(output_file_name->c_str(), "wb")
                         : std::freopen(nullptr, "wb", stdout), &std::fclose);
}
